package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.vrgsoft.numerologiaclean.presentation.base.BaseFragment
import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel
import org.kodein.di.generic.instance

class AddUserFragment : BaseFragment<ViewDataBinding>() {
    override val kodeinModule = AddUserModule.module(this)
    override val viewModel: BaseViewModel by instance()

    val di = AddUserModule.module(this)

    override fun viewCreated(savedInstanceState: Bundle?) {
    }

    companion object {
        fun newInstance() = AddUserFragment()
    }
}