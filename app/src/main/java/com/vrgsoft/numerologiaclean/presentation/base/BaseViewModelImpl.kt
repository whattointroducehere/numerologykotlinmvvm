package com.vrgsoft.numerologiaclean.presentation.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModelImpl : ViewModel(), BaseViewModel {

    override fun bound() {}
    override fun unbound() {}
}
