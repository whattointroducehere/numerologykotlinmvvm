package com.vrgsoft.numerologiaclean.presentation.activity_main

import com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser.AddUserContract
import com.vrgsoft.numerologiaclean.presentation.example.ExampleContract

class MainRouter (
        private val activity: MainActivity
) : ExampleContract.Router,
AddUserContract.Router{

    override fun goBack() {
              activity.onBackPressed()
        }


//
//    fun navigateToExampleFragment() {
//        activity.supportFragmentManager.replaceWithoutBackStack(R.id.container, ExampleFragment.newInstance())
//    }



}
