package com.vrgsoft.numerologiaclean.presentation.utils.network

import android.content.Context
import retrofit2.HttpException
import retrofit2.Response

class NetworkErrorHandler(appContext: Context) : com.vrgsoft.numerologiaclean.data.repository.example.common.RemoteErrorHandler {

    override fun <T> handleError(response: Response<T>): Throwable {
        val code = response.code()

        return when (code) {
            401 -> com.vrgsoft.numerologiaclean.data.model.exception.AuthException()
            else -> HttpException(response)
        }
    }

}
