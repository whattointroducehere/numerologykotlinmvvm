package com.vrgsoft.numerologiaclean.domain.base

import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class BaseInteractor {
    private val jobThread: Scheduler = Schedulers.io()
    private val observeThread: Scheduler = AndroidSchedulers.mainThread()

    protected val disposables = CompositeDisposable()

    protected fun <T> applySingleSchedulers(): SingleTransformer<T, T> {
        return SingleTransformer {
            it.subscribeOn(jobThread).observeOn(observeThread)
        }
    }

    protected fun <T> applyObservableSchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer {
            it.subscribeOn(jobThread).observeOn(observeThread)
        }
    }

    protected fun applyCompletableSchedulers(): CompletableTransformer {
        return CompletableTransformer {
            it.subscribeOn(jobThread).observeOn(observeThread)
        }
    }

    fun dispose() = disposables.dispose()

    fun clear() = disposables.clear()

}
