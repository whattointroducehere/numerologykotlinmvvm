package com.vrgsoft.numerologiaclean.presentation.loginactivity

import com.vrgsoft.numerologiaclean.presentation.activity_main.MainActivity

class LoginRouter(private val activity : LoginActivity) : LoginContract.Router {
    override fun openDate() {


    }

    override fun openMainScreen() {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finish()


    }


}