package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_settings

import com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_compatibility.CompatibilityViewModel
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

object SettingsModule {
    fun module(fragment: SettingsFragment) = Kodein.Module("SettingsModule"){
        bind<SettingsFactory>() with provider { SettingsFactory(instance()) }
        bind<SettingsContract.ViewModel>() with singleton { fragment.vm<SettingsViewModel>(instance()) }
    }
}