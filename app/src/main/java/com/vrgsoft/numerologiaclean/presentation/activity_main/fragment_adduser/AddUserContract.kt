package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser

import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel

interface AddUserContract {

    interface ViewModel : BaseViewModel {

        fun goBack()

        fun uploadAvatar()
    }


    interface Router {
        fun goBack()

    }
}