package com.vrgsoft.numerologiaclean.data.repository.example

import io.reactivex.Single

interface ExampleRepository {
    fun exampleQuery(query: String): Single<List<com.vrgsoft.numerologiaclean.data.model.Example>>
}
