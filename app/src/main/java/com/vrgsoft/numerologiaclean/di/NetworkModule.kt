package com.vrgsoft.numerologiaclean.di

import android.content.Context
import com.example.marsrobots.BuildConfig
//import com.example.marsrobots.presentation.utils.network.BaseHeaderInterceptor
//import com.example.marsrobots.presentation.utils.network.BearerAuthenticator
//import com.example.marsrobots.presentation.utils.network.ErrorMessageFabric
//import com.example.marsrobots.presentation.utils.network.NetworkErrorHandler
import com.vrgsoft.numerologiaclean.presentation.utils.network.BaseHeaderInterceptor
import com.vrgsoft.numerologiaclean.presentation.utils.network.BearerAuthenticator
import com.vrgsoft.numerologiaclean.presentation.utils.network.ErrorMessageFabric
import com.vrgsoft.numerologiaclean.presentation.utils.network.NetworkErrorHandler
import okhttp3.Authenticator
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun networkModule(context: Context) = Kodein.Module("networkModule") {

    bind<com.vrgsoft.numerologiaclean.data.repository.example.common.RemoteErrorHandler>() with singleton { NetworkErrorHandler(context) }
    bind<ErrorMessageFabric>() with singleton { ErrorMessageFabric(context) }

    bind<com.vrgsoft.numerologiaclean.data.api.service.ExampleService>() with singleton { instance<Retrofit>().create(
            com.vrgsoft.numerologiaclean.data.api.service.ExampleService::class.java) }

    bind<BaseHeaderInterceptor>() with singleton { BaseHeaderInterceptor(context) }

    bind<Authenticator>() with singleton { BearerAuthenticator(context) }

    bind<Retrofit>() with singleton {
        Retrofit.Builder()
                .baseUrl(com.vrgsoft.numerologiaclean.Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(instance())
                .build()
    }

    bind<OkHttpClient>() with singleton {
        val builder = OkHttpClient.Builder()

        builder.cache(instance())

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }

        builder.addNetworkInterceptor(instance<BaseHeaderInterceptor>())
        builder.retryOnConnectionFailure(true)
        builder.authenticator(instance())
        builder.readTimeout(50, TimeUnit.SECONDS)
        builder.writeTimeout(50, TimeUnit.SECONDS)
        builder.build()
    }

    bind() from provider {
        val cacheSize = 10 * 1024 * 1024 // 10 MB
        Cache(context.cacheDir, cacheSize.toLong())
    }

}
