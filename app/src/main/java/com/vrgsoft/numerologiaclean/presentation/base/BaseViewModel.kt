package com.vrgsoft.numerologiaclean.presentation.base


interface BaseViewModel {
    fun bound()

    fun unbound()
}