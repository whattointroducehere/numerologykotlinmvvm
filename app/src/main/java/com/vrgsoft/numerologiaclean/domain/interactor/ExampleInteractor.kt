package com.vrgsoft.numerologiaclean.domain.interactor
//
//import com.example.marsrobots.domain.base.BaseInteractor
//import com.example.marsrobots.domain.rx.SingleSubscriber
//import com.example.marsrobots.domain.rx.applyIOSchedulers
//import com.example.marsrobots.domain.rx.applySubscriber
//import com.example.marsrobots.domain.usecase.ExampleUsecase
import com.vrgsoft.numerologiaclean.data.model.Example
import com.vrgsoft.numerologiaclean.domain.base.BaseInteractor
import com.vrgsoft.numerologiaclean.domain.rx.SingleSubscriber
import com.vrgsoft.numerologiaclean.domain.rx.applyIOSchedulers
import com.vrgsoft.numerologiaclean.domain.rx.applySubscriber
import com.vrgsoft.numerologiaclean.domain.usecase.ExampleUsecase
import io.reactivex.disposables.Disposable

class ExampleInteractor(private val exampleRepository: com.vrgsoft.numerologiaclean.data.repository.example.ExampleRepository
) : BaseInteractor(), ExampleUsecase {

    override fun example(query: String, subscriber: SingleSubscriber<List<Example>>): Disposable {
        return exampleRepository.exampleQuery(query)
                .compose(applySingleSchedulers())
                .applyIOSchedulers()
                .applySubscriber(subscriber)
                .apply { disposables.add(this) }
    }
}
