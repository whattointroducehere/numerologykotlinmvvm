package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_compatibility

import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel

interface CompatibilityContract {

    interface ViewModel : BaseViewModel

    interface Router
}