package com.vrgsoft.numerologiaclean.presentation.example

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.example.marsrobots.R
import com.vrgsoft.numerologiaclean.presentation.base.BaseFragment
import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class ExampleFragment : BaseFragment<ViewDataBinding>() {
    override val kodeinModule = ExampleModule.module(this)
    override val viewModel: BaseViewModel by instance()

    val di = ExampleModule.module(this)

    override fun viewCreated(savedInstanceState: Bundle?) {
    }

    companion object {
        fun newInstance() = ExampleFragment()
    }
}
