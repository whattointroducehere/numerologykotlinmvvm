package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_settings

import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel

interface SettingsContract {

    interface ViewModel : BaseViewModel

    interface Router
}