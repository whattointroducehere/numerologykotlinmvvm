package com.vrgsoft.numerologiaclean.presentation.utils.extintions

import android.annotation.SuppressLint
import com.example.marsrobots.R

@SuppressLint("CheckResult")
fun androidx.fragment.app.FragmentManager.replaceWithoutBackStack(containerId: Int,
        fragment: androidx.fragment.app.Fragment) {
    beginTransaction()
            .replace(containerId, fragment, fragment::class.java.simpleName)
            .commit()
}

fun androidx.fragment.app.FragmentManager.replaceWithBackStack(containerId: Int,
        fragment: androidx.fragment.app.Fragment) {
    beginTransaction()
            .replace(containerId, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
}

@SuppressLint("CheckResult")
fun androidx.fragment.app.FragmentManager.addFragment(containerId: Int,
        fragment: androidx.fragment.app.Fragment) {
    beginTransaction()
            .add(containerId, fragment, fragment::class.java.simpleName)
            .commit()
}

@SuppressLint("CheckResult")
fun androidx.fragment.app.FragmentManager.addFragmentWithBackStack(containerId: Int,
        fragment: androidx.fragment.app.Fragment) {
    beginTransaction()
            .add(containerId, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
}

@SuppressLint("CheckResult")
fun androidx.fragment.app.FragmentManager.addFragmentWithBackStackAnimDownUp(containerId: Int,
        fragment: androidx.fragment.app.Fragment) {
    beginTransaction()
            .setCustomAnimations(R.animator.fragment_enter, R.animator.fragment_exit,
                    R.animator.fragment_enter, R.animator.fragment_exit)
            .add(containerId, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
}

