package com.vrgsoft.numerologiaclean.data.repository.example.common

import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.Maybe
import io.reactivex.MaybeTransformer
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.SingleTransformer
import retrofit2.Response

open class BaseRemoteStorage(protected val errorHandler: com.vrgsoft.numerologiaclean.data.repository.example.common.RemoteErrorHandler) : com.vrgsoft.numerologiaclean.data.repository.example.common.BaseStorage.Remote {

    fun <T> applySingleResponseExceptionHandler(): SingleTransformer<Response<T>, T> {
        return SingleTransformer { single ->
            return@SingleTransformer single.flatMap {
                if (!it.isSuccessful) {
                    Single.error(errorHandler.handleError(it))
                } else {
                    Single.just(it.body())
                }
            }
        }
    }

    fun <T> applyObservableResponseExceptionHandler(): ObservableTransformer<Response<T>, T> {
        return ObservableTransformer { observable ->
            return@ObservableTransformer observable.flatMap {
                if (!it.isSuccessful) {
                    Observable.error(errorHandler.handleError(it))
                } else {
                    Observable.just(it.body())
                }
            }
        }
    }

    fun <T> applyFlowableCompletableResponseExceptionHandler(): FlowableTransformer<Response<T>, T> {
        return FlowableTransformer { flowable ->
            return@FlowableTransformer flowable.flatMap {
                if (!it.isSuccessful) {
                    Flowable.error(errorHandler.handleError(it))
                } else {
                    Flowable.just(it.body())
                }
            }
        }
    }

    fun <T> applyMaybeCompletableResponseExceptionHandler(): MaybeTransformer<Response<T>, T> {
        return MaybeTransformer { maybe ->
            return@MaybeTransformer maybe.flatMap {
                if (!it.isSuccessful) {
                    Maybe.error(errorHandler.handleError(it))
                } else {
                    Maybe.just(it.body())
                }
            }
        }
    }
}
