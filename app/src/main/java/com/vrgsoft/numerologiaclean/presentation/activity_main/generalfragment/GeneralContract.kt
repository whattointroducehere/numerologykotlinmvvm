package com.vrgsoft.numerologiaclean.presentation.activity_main.generalfragment

import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel

interface GeneralContract {

    interface ViewModel : BaseViewModel


    interface Router
}