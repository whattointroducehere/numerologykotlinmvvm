package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class AddUserFactory(private val router: AddUserContract.Router) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        AddUserViewModel(router) as T
}