package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_compatibility

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

object CompatibilityModule {
    fun module(fragment: CompatibilityFragment) = Kodein.Module("CompatibilityModule"){
        bind<CompatibilityFactory>() with provider { CompatibilityFactory(instance()) }
        bind<CompatibilityContract.ViewModel>() with singleton { fragment.vm<CompatibilityViewModel>(instance()) }
    }
}