package com.vrgsoft.numerologiaclean.presentation.example

//import com.example.marsrobots.presentation.base.BaseContract
import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel

interface ExampleContract {

    interface ViewModel : BaseViewModel {
        fun loadData()

    }

    interface Router {
    }
}
