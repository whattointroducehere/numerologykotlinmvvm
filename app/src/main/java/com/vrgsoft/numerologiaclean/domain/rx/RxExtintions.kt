package com.vrgsoft.numerologiaclean.domain.rx

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

inline fun <reified T> Observable<T>.applyIOSchedulers(): Observable<T> =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

inline fun <reified T> Single<T>.applyIOSchedulers(): Single<T> =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

inline fun <reified T> Flowable<T>.applyIOSchedulers(): Flowable<T> =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

inline fun Completable.applyIOSchedulers(): Completable =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

inline fun <reified T> Single<T>.applySubscriber(subscriber: SingleSubscriber<T>) =
        this.doOnSubscribe(subscriber.progressStart)
                .doFinally(subscriber.progressEnd)
                .subscribe(subscriber.onNext, subscriber.onError)

inline fun Completable.applySubscriber(subscriber: CompletableSubscriber) =
        this.doOnSubscribe(subscriber.progressStart)
                .doFinally(subscriber.progressEnd)
                .subscribe(subscriber.onComplete, subscriber.onError)

inline fun <reified T> Observable<T>.applySubscriber(subscriber: ObservableSubscriber<T>) =
        this.doOnSubscribe(subscriber.progressStart)
                .doAfterNext({ subscriber.progressEnd })
                .subscribe(subscriber.onNext, subscriber.onError)
