package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_settings

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_compatibility.CompatibilityModule
import com.vrgsoft.numerologiaclean.presentation.base.BaseFragment
import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel
import org.kodein.di.generic.instance

class SettingsFragment : BaseFragment<ViewDataBinding>() {
    override fun viewCreated(savedInstanceState: Bundle?) {

    }

    override val kodeinModule = SettingsModule.module(this)
    override val viewModel: BaseViewModel by instance()

    val di = SettingsModule.module(this)
}