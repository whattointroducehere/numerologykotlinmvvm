package com.vrgsoft.numerologiaclean.data.repository.example.common

interface CacheableStorage<T> {
    fun cacheData(data: List<T>)
}
