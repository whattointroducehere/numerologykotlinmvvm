package com.vrgsoft.numerologiaclean.presentation.utils.network

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

class BaseHeaderInterceptor(context: Context) : Interceptor, KodeinAware {
    override val kodein: Kodein  by closestKodein(context)

    //private val accountUsecase: AccountUsecase by instance()

    override fun intercept(chain: Interceptor.Chain): Response {
        var original = chain.request()

        //val authToken = accountUsecase.getToken()

//        if (authToken != null) {
//            val requestBuilder = original.newBuilder()
//                    ?.removeHeader(Constants.TOKEN_HEADER_NAME)
//                    ?.addHeader(Constants.TOKEN_HEADER_NAME, "${BearerAuthenticator.TOKEN_HEADER_PREFIX} $authToken")
//            original = requestBuilder?.build()
//        }

        return chain.proceed(original)
    }
}
