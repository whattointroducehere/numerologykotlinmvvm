package com.vrgsoft.numerologiaclean.di

//import com.example.marsrobots.domain.interactor.ExampleInteractor
//import com.example.marsrobots.domain.usecase.ExampleUsecase
import com.vrgsoft.numerologiaclean.domain.interactor.ExampleInteractor
import com.vrgsoft.numerologiaclean.domain.usecase.ExampleUsecase
import org.kodein.di.Kodein
import org.kodein.di.Kodein.Module
import org.kodein.di.android.androidModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

object AppModule {
    fun module(application: com.vrgsoft.numerologiaclean.App) = Kodein.Module("AppModule") {
        import(androidModule(application))
        import(networkModule(application))


        import(usecaseModule())
    }

    private fun remoteStorageModule() = Module("module name") {
        bind<com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleStorage.Remote>() with singleton {
            com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleRemoteStorage(instance(), instance())
        }
    }

    private fun localStorageModule() = Module("module name") {
        bind<com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleStorage.Local>() with singleton { com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleLocalStorage() }
    }

    private fun storageModule() = Module("module name") {
        import(remoteStorageModule())
        import(localStorageModule())
    }

    private fun repositoryModule() = Kodein.Module("repositoryModule") {
        import(storageModule())

        bind<com.vrgsoft.numerologiaclean.data.repository.example.ExampleRepository>() with singleton {
            com.vrgsoft.numerologiaclean.data.repository.example.ExampleRepositoryImpl(instance(), instance())
        }
    }

    private fun usecaseModule() = Kodein.Module("usecaseModule") {
        import(repositoryModule())

        bind<ExampleUsecase>() with provider { ExampleInteractor(instance()) }
    }
}
