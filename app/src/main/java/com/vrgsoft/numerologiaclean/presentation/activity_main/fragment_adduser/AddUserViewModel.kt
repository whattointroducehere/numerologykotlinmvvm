package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser

import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModelImpl

class AddUserViewModel(private val router : AddUserContract.Router) : BaseViewModelImpl(),AddUserContract.ViewModel{
    override fun uploadAvatar() {

    }

    override fun goBack() {
        router.goBack()
    }


}