package com.vrgsoft.numerologiaclean.domain.base

interface BaseUsecase {
    fun clear()

    fun dispose()
}
