package com.vrgsoft.numerologiaclean.domain.usecase

//import com.example.marsrobots.domain.base.BaseUsecase
//import com.example.marsrobots.domain.rx.SingleSubscriber
import com.vrgsoft.numerologiaclean.data.model.Example
import com.vrgsoft.numerologiaclean.domain.base.BaseUsecase
import com.vrgsoft.numerologiaclean.domain.rx.SingleSubscriber
import io.reactivex.disposables.Disposable

interface ExampleUsecase : BaseUsecase {
    fun example(query: String, subscriber: SingleSubscriber<List<Example>>): Disposable
}
