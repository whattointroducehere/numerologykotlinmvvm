package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_compatibility

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.vrgsoft.numerologiaclean.presentation.base.BaseFragment
import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel
import org.kodein.di.generic.instance

class CompatibilityFragment : BaseFragment<ViewDataBinding>() {
    override val kodeinModule = CompatibilityModule.module(this)
    override val viewModel: BaseViewModel by instance()

    val di = CompatibilityModule.module(this)

    override fun viewCreated(savedInstanceState: Bundle?) {
    }

    companion object {
        fun newInstance() = CompatibilityFragment()
    }
}