package com.vrgsoft.numerologiaclean.presentation.splashactivity

import android.content.Intent
import android.os.Bundle
import com.example.marsrobots.R
import com.vrgsoft.numerologiaclean.presentation.base.BaseActivity
import com.vrgsoft.numerologiaclean.presentation.loginactivity.LoginActivity
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class SplashActivity : BaseActivity() {
    override fun diModule() = Kodein.Module("Splash module") {
        bind() from singleton { SplashRouter(this@SplashActivity) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        val background = object : Thread(){
            override fun run() {
                try{
                    Thread.sleep(5000)

         //          val intent = Intent(baseContext,LoginActivity::class.java)
                    val intent = LoginActivity.newIntent(this@SplashActivity)
                    startActivity(intent)
                    finish()
                }catch (e:ExceptionInInitializerError){
                    e.printStackTrace()
                }
            }
        }

        background.start()


    }
}