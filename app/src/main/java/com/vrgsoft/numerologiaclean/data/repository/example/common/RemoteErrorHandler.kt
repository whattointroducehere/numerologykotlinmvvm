package com.vrgsoft.numerologiaclean.data.repository.example.common

import retrofit2.Response

interface RemoteErrorHandler {
    fun <T> handleError(response: Response<T>): Throwable
}
