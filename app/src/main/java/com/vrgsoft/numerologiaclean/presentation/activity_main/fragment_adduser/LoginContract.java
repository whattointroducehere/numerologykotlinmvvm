package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser;

public interface LoginContract {

    interface View{
        void switchLoginButtonState(boolean isEnabled);

    }

    interface Presenter{

    }

    interface Router{

    }
}
