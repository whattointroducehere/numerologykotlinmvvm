package com.vrgsoft.numerologiaclean.presentation.utils

import android.text.Editable
import android.text.TextWatcher

fun afterTextChanged(afterTextChanged: (text: String) -> Unit): TextWatcher {
    return object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            afterTextChanged(s.toString())
        }
    }
}
