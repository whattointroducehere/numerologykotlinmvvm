package com.vrgsoft.numerologiaclean.presentation.utils.extintions

import android.animation.ValueAnimator
import android.view.View
import android.widget.FrameLayout

fun View.collapse(duration: Int) {
    animateViewHeightChange(this, duration, measuredHeight, 0)
}

fun View.expand(duration: Int) {
    measure(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT)
    animateViewHeightChange(this, duration, 0, measuredHeight)
}

private fun animateViewHeightChange(view: View, duration: Int, from: Int, to: Int) {
    val valueAnimator = ValueAnimator.ofInt(from, to)
    valueAnimator.setDuration(duration.toLong())
            .addUpdateListener { animation ->
                val value = animation.animatedValue as Int

                view.layoutParams.height = value
                view.requestLayout()

                if (to == 0 && value == 0) {
                    view.visibility = View.GONE
                }

                if (from == 0 && value > 0) {
                    view.visibility = View.VISIBLE
                }
            }
    valueAnimator.start()
}

fun View.setVisibility(flag: Boolean) {
    visibility = if (flag) View.VISIBLE else View.GONE
}
