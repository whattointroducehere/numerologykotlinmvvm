package com.vrgsoft.numerologiaclean.presentation.utils.network

import android.content.Context
import com.example.marsrobots.R
import java.net.UnknownHostException

class ErrorMessageFabric(private val context: Context) {

    fun getMessage(throwable: Throwable): Int? {
        if (throwable is UnknownHostException) {
            return R.string.network_error
        } else {
            return R.string.unknown_error
        }
    }
}
