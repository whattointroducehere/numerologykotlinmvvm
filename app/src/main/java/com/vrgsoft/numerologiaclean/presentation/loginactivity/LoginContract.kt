package com.vrgsoft.numerologiaclean.presentation.loginactivity

import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel

interface LoginContract {

    interface ViewModel : BaseViewModel {

        fun logginClick()

        fun dateClick()

        fun onUploadAvatarClick()


    }


    interface Router{
        fun openMainScreen()

        fun openDate()

    }
}