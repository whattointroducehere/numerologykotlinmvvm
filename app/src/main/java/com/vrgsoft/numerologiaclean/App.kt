package com.vrgsoft.numerologiaclean

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger

class App : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(com.vrgsoft.numerologiaclean.di.AppModule.module(this@App))
    }
    override val kodeinTrigger = KodeinTrigger()

    override fun onCreate() {
        super.onCreate()
        kodeinTrigger.trigger()
    }
}
