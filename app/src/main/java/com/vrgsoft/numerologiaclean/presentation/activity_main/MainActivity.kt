package com.vrgsoft.numerologiaclean.presentation.activity_main


import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.marsrobots.R
import com.example.marsrobots.databinding.ActivityMainBinding
import com.vrgsoft.numerologiaclean.presentation.base.BaseActivity
import org.kodein.di.Kodein.Module
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class MainActivity : BaseActivity() {
    override fun diModule() = Module("module name") {
        bind() from singleton { MainRouter(this@MainActivity) }
    }

    private lateinit var binding: ActivityMainBinding
    private val router: MainRouter by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    override fun onBackPressed() {
//        if (binding.draggableRightMenu.isOpened()) {
//            binding.draggableRightMenu.close()
//        } else {
//            val fragmentManager = supportFragmentManager
//            val isStateSaved = fragmentManager.isStateSaved
//            if (isStateSaved && Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1) {
//                return
//            }
//            if (fragmentManager.isStateSaved || !fragmentManager.popBackStackImmediate()) {
//                val startMain = Intent(Intent.ACTION_MAIN)
//                startMain.addCategory(Intent.CATEGORY_HOME)
//                startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//                startActivity(startMain)
//            }
//        }
    }







}
