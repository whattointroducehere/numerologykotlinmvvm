package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_compatibility

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser.AddUserContract
import com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser.AddUserViewModel

@Suppress("UNCHECKED_CAST")
class CompatibilityFactory(private val router: AddUserContract.Router) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        AddUserViewModel(router) as T
}