package com.vrgsoft.numerologiaclean.data.repository.example.storage

import io.reactivex.Single

class ExampleRemoteStorage(
    errorHandler: com.vrgsoft.numerologiaclean.data.repository.example.common.RemoteErrorHandler,
    private val exampleService: com.vrgsoft.numerologiaclean.data.api.service.ExampleService
) : com.vrgsoft.numerologiaclean.data.repository.example.common.BaseRemoteStorage(errorHandler), com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleStorage.Remote {

    override fun example(query: String): Single<List<com.vrgsoft.numerologiaclean.data.model.Example>> {
        return exampleService.example(query)
                .compose(applySingleResponseExceptionHandler())
                .toObservable()
                .flatMapIterable { it }
                .map { it.example }
                .toList()
    }

}
