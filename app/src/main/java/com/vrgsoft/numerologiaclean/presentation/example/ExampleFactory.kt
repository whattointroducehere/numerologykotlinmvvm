package com.vrgsoft.numerologiaclean.presentation.example

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class ExampleFactory(private val router: ExampleContract.Router) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
            ExampleViewModel(router) as T
}