package com.vrgsoft.numerologiaclean.presentation.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
//import android.support.annotation.Nullable
//import android.support.v7.app.AppCompatActivity
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.android.closestKodein
import org.kodein.di.android.retainedKodein
import java.util.*

abstract class BaseActivity : AppCompatActivity(), KodeinAware{
    private val _parentKodein by closestKodein()
    override val kodein: Kodein by retainedKodein {
        extend(_parentKodein)
        import(diModule())
    }
    override val kodeinTrigger = KodeinTrigger()

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        kodeinTrigger.trigger()
        super.onCreate(savedInstanceState)
    }

    abstract fun diModule(): Kodein.Module

//    override fun updateTitle(title: Int) {
//        if(title != 0){
//            updateTitleRes(title)
//        }
//    }
//
//    override fun backClicked() {
//        onBackPressed()
//    }
//
//    override fun homeClicked() {
//        showDrawer()
//    }

    open fun updateTitleRes(title: Int){}
    open fun showDrawer(){}

    protected fun transactionActivity(activity: Class<*>?, cycleFinish: Boolean) {
        if (activity != null) {
            val intent = Intent(this, activity)
            intent.removeExtra("cmd")
            startActivity(intent)
            if (cycleFinish) {
                this.finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }



}
