package com.vrgsoft.numerologiaclean.data.repository.example.storage

import io.reactivex.Single

class ExampleLocalStorage : com.vrgsoft.numerologiaclean.data.repository.example.common.BaseLocalStorage(), com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleStorage.Local {
    override fun exampleFromCache(query: String): Single<List<com.vrgsoft.numerologiaclean.data.model.Example>> {
        return Single.just(listOf())
    }

    override fun cacheData(data: List<com.vrgsoft.numerologiaclean.data.model.Example>) {
        //TODO example
    }

}
