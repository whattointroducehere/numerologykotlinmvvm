package com.vrgsoft.numerologiaclean.data.api.service

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ExampleService {

    @GET("search?media_type=image")
    fun example(@Query("q") query: String): Single<Response<List<com.vrgsoft.numerologiaclean.data.api.response.ExampleResponse>>>

}
