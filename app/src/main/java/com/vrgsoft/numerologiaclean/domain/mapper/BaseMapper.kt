package com.vrgsoft.numerologiaclean.domain.mapper

interface BaseMapper<T, V> {
    fun map(entity: T): V
}
