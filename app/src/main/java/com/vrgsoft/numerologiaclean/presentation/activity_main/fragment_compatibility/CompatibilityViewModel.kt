package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_compatibility

import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModelImpl

class CompatibilityViewModel(private val router : CompatibilityContract.Router) : BaseViewModelImpl(), CompatibilityContract.ViewModel {
}