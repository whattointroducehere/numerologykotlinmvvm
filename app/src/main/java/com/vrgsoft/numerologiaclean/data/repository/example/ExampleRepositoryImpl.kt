package com.vrgsoft.numerologiaclean.data.repository.example

import io.reactivex.Single

class ExampleRepositoryImpl(
    private val local: com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleStorage.Local,
    private val remote: com.vrgsoft.numerologiaclean.data.repository.example.storage.ExampleStorage.Remote
) : com.vrgsoft.numerologiaclean.data.repository.example.ExampleRepository {

    override fun exampleQuery(query: String): Single<List<com.vrgsoft.numerologiaclean.data.model.Example>> {
        return remote.example(query)
                .doAfterSuccess { local.cacheData(it) }
                .onErrorResumeNext { local.exampleFromCache(query) }

    }
}
