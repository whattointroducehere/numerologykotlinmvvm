package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class SettingsFactory(private val router: SettingsContract.Router) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        SettingsViewModel(router) as T
}