package com.vrgsoft.numerologiaclean.domain.rx

import io.reactivex.disposables.Disposable

class ObservableSubscriber<T>(
        val onNext: (T) -> Unit = {},
        val onError: (Throwable) -> Unit = {},
        val progressStart: (Disposable) -> Unit = {},
        val progressEnd: () -> Unit = {}
)
