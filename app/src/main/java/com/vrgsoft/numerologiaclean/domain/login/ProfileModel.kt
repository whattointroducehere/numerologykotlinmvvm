package com.vrgsoft.numerologiaclean.domain.login

class ProfileModel {
}
//public class ProfileModel extends RealmObject implements Parcelable {
//
//    @PrimaryKey
//    private int id;
//    private String email;
//    @Nullable
//    private String firstName;
//    @Nullable
//    private String lastName;
//    @Nullable
//    private String avatarUrl;
//
//    public ProfileModel() {
//    }
//
//    public ProfileModel(int id, String email, @Nullable String firstName, @Nullable String lastName,
//                        @Nullable String avatarUrl) {
//        this.id = id;
//        this.email = email;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.avatarUrl = avatarUrl;
//    }
//
//    protected ProfileModel(Parcel in) {
//        id = in.readInt();
//        email = in.readString();
//        firstName = in.readString();
//        lastName = in.readString();
//        avatarUrl = in.readString();
//    }
//
//    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
//        @Override
//        public ProfileModel createFromParcel(Parcel in) {
//            return new ProfileModel(in);
//        }
//
//        @Override
//        public ProfileModel[] newArray(int size) {
//            return new ProfileModel[size];
//        }
//    };
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    @Nullable
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(@Nullable String firstName) {
//        this.firstName = firstName;
//    }
//
//    @Nullable
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(@Nullable String lastName) {
//        this.lastName = lastName;
//    }
//
//    @Nullable
//    public String getAvatarUrl() {
//        return avatarUrl;
//    }
//
//    public void setAvatarUrl(@Nullable String avatarUrl) {
//        this.avatarUrl = avatarUrl;
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeInt(id);
//        dest.writeString(email);
//        dest.writeString(firstName);
//        dest.writeString(lastName);
//        dest.writeString(avatarUrl);
//    }
//
//    public boolean isFilled() {
//        return !TextUtils.isEmpty(firstName);
//    }
//}
//
