package com.vrgsoft.numerologiaclean.presentation.utils.network

import android.annotation.SuppressLint
import android.content.Context
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

class BearerAuthenticator(context: Context) : Authenticator, KodeinAware {
    private val _parentKodein: Kodein by closestKodein(context)

    override val kodein: Kodein = Kodein.lazy {
        extend(_parentKodein)
    }

    //private val accountUsecase: AccountUsecase by instance()

    @SuppressLint("CheckResult")
    override fun authenticate(route: Route?, response: Response?): Request? {
//        if (response?.code() == 401 && accountUsecase.isTokenExpired()) {
//            try {
//                accountUsecase.refreshToken().blockingGet()
//            } catch (e: Exception) {
//                if (e is HttpException) {
//                    if (e.code() == 400) {
//                        accountUsecase.logout().subscribe({}, Timber::e)
//                    }
//                }
//                return null
//            }
//        }
//
//        val authToken = accountUsecase.getToken()
//        val currentSport = accountUsecase.getSportCode()
//        return response?.request()?.newBuilder()
//                ?.removeHeader(Constants.TOKEN_HEADER_NAME)
//                ?.addHeader(Constants.TOKEN_HEADER_NAME, "$TOKEN_HEADER_PREFIX $authToken")
//                ?.build()

        return response?.request()?.newBuilder()?.build()
    }

    companion object {
        const val TOKEN_HEADER_PREFIX = "Bearer"
    }

}
