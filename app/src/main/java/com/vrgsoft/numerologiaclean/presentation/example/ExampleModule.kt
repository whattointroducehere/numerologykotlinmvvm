package com.vrgsoft.numerologiaclean.presentation.example

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

object ExampleModule {
    fun module(fragment: ExampleFragment) = Kodein.Module("ExampleModule"){
        bind<ExampleFactory>() with provider { ExampleFactory(instance()) }
        bind<ExampleContract.ViewModel>() with singleton { fragment.vm<ExampleViewModel>(instance()) }
    }
}