package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

object AddUserModule {
    fun module(fragment: AddUserFragment) = Kodein.Module("AddUserModule"){
        bind<AddUserFactory>() with provider { AddUserFactory(instance()) }
        bind<AddUserContract.ViewModel>() with singleton { fragment.vm<AddUserViewModel>(instance()) }
    }
}