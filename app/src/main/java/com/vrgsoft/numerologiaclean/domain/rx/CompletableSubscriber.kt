package com.vrgsoft.numerologiaclean.domain.rx

import io.reactivex.disposables.Disposable

class CompletableSubscriber(
        val onComplete: () -> Unit = {},
        val onError: (Throwable) -> Unit = {},
        val progressStart: (Disposable) -> Unit = {},
        val progressEnd: () -> Unit = {}
)
