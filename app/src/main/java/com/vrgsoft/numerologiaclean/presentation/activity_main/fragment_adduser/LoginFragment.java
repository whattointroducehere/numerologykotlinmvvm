package com.vrgsoft.numerologiaclean.presentation.activity_main.fragment_adduser;

public class LoginFragment implements LoginContract.View {


    @Override
    public void switchLoginButtonState(boolean isEnabled) {

    }

    public interface LoginClickListener {

        void onLoginClick(String email, String password);

        void onForgotPasswordClick();

        void onSignupClick();
    }
}
