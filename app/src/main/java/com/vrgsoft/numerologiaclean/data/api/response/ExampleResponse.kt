package com.vrgsoft.numerologiaclean.data.api.response

import com.google.gson.annotations.SerializedName

data class ExampleResponse(
        @SerializedName("href") var href: String?,
        @SerializedName("metadata") var metadata: Metadata?,
        @SerializedName("version") var version: String?,
        var example: com.vrgsoft.numerologiaclean.data.model.Example
)
