package com.vrgsoft.numerologiaclean.data.repository.example.common

interface BaseStorage {
    interface Local

    interface Remote
}
