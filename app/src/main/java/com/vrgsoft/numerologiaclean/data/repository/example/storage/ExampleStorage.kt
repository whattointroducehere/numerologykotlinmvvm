package com.vrgsoft.numerologiaclean.data.repository.example.storage

import io.reactivex.Single

interface ExampleStorage {
    interface Local : com.vrgsoft.numerologiaclean.data.repository.example.common.BaseStorage.Local, com.vrgsoft.numerologiaclean.data.repository.example.common.CacheableStorage<com.vrgsoft.numerologiaclean.data.model.Example> {
        fun exampleFromCache(query: String): Single<List<com.vrgsoft.numerologiaclean.data.model.Example>>
    }

    interface Remote : com.vrgsoft.numerologiaclean.data.repository.example.common.BaseStorage.Remote {
        fun example(query: String): Single<List<com.vrgsoft.numerologiaclean.data.model.Example>>
    }
}
