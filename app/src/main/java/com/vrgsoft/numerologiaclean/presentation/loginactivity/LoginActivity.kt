package com.vrgsoft.numerologiaclean.presentation.loginactivity

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.marsrobots.R
import com.example.marsrobots.databinding.ActivityLoginBinding
import com.vrgsoft.numerologiaclean.presentation.base.BaseActivity
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import java.util.*

class LoginActivity : BaseActivity() {


    override fun diModule() = Kodein.Module("Login module") {
        bind() from provider { LoginRouter(this@LoginActivity) }
        bind<LoginContract.ViewModel>() with singleton { LoginViewModel(instance()) }
    }

    private val router: LoginRouter by instance()
    private val viewModel: LoginContract.ViewModel by instance()
    private var editBirthday: TextView? = null
    private lateinit var binding: ActivityLoginBinding
    private var dateSetListener: DatePickerDialog.OnDateSetListener? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_login)
        setContentView(R.layout.activity_login)
        editBirthday = findViewById(R.id.editBirthday) as TextView
        editBirthday!!.setOnClickListener(View.OnClickListener {
            val cal = Calendar.getInstance()
            val year = cal.get(Calendar.YEAR)
            val month = cal.get(Calendar.MONTH)
            val day = cal.get(Calendar.DAY_OF_MONTH)


            val dialog = DatePickerDialog(
                this@LoginActivity,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                dateSetListener,
                year, month, day
            )
           dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        })

        dateSetListener = DatePickerDialog.OnDateSetListener { view, year, month, day ->
            var month = month
            month = month + 1

            val date = "$month/$day/$year"

            editBirthday!!.setText(date)
        }
        binding.viewModel = this@LoginActivity.viewModel
        viewModel.bound()



    }


    companion object {
        @JvmStatic
        fun newIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }


    fun hideKeyboard() {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        Objects.requireNonNull(imm).hideSoftInputFromWindow(this.window.decorView.windowToken, 0)

    }







}