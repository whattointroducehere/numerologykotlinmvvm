package com.vrgsoft.numerologiaclean.presentation.splashactivity

import com.vrgsoft.numerologiaclean.presentation.base.BaseContract
import com.vrgsoft.numerologiaclean.presentation.base.BaseViewModel

interface SplashContract {

    interface ViewModel : BaseViewModel



    interface Router{

        fun goToLoginFragment()

    }

}