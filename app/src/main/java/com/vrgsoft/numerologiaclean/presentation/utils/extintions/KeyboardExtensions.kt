package com.vrgsoft.numerologiaclean.presentation.utils.extintions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun Fragment.showKeyboard(view: View) {
    val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

    inputManager?.showSoftInput(view, 0)
}

fun Fragment.hideKeyboard() {
    val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

    if (view != null) inputManager?.hideSoftInputFromWindow(view?.windowToken, 0)
}
