package com.vrgsoft.numerologiaclean.presentation.base

abstract class BasePresenter<T : BaseContract.View> : BaseContract.Presenter<T> {

    protected var view: T? = null

    override fun attachView(view: T) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}
